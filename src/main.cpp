#include "opencv2/opencv.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv/cv.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <string>
#define DEBUG

using namespace cv;
using std::cout;
using std::endl;
using std::vector;
Mat img, imgHSV, imgGray, imgDebug;
Mat hsv[3];
Mat result;
std::vector<Mat> detectedWorms;
std::vector<Mat> detectedDiscs;
std::vector<Mat> detectedBears;

std::vector<Rect> boundRect;
std::vector<Point2f>center;
std::vector<float>radius;

float scaleSum = 0;
float scale;

int CannyLowThreshold = 28;
int bears = 0;
int worms = 0;
int discs = 0;
int bearsTypes[6];	// 0 - ciemnyCzerwony	1-jasnyCzerwony 2-pomaranczowy	3-zolty	4-zielony	5-bialy
int wormsTypes[3];	// 0 - zielono-bialy	1 - pomaranczowo-czarny	2 - zolto-czerwony
int discsTypes[6];	// 0 - ciemnyCzerwony	1-jasnyCzerwony	2-pomaranczowy	3-zolty 4-zielony	5-bialy

int countPixels(Mat);
std::vector<int> jellyColor(Mat);
void printColors(std::vector<int>);
void Erosion(Mat, Mat);
void Dilation(Mat, Mat);

void FindZelki(int, void*)
{
	Mat edges;
	Mat res;
	std::vector<std::vector<Point> > contours;
	Canny(imgGray, edges, CannyLowThreshold, CannyLowThreshold * 3);		//Zaznaczanie krawedzi
	//threshold(imgGray, edges, CannyLowThreshold, CannyLowThreshold * 40, CV_THRESH_BINARY);
	int size = 3;
	//aerode(edges, edges,getStructuringElement( MORPH_RECT, Size( 2*size + 1, 2*size+1 ), Point( size, size ) ));
	dilate(edges, edges, getStructuringElement(MORPH_RECT, Size(2 * size + 1, 2 * size + 1), Point(size, size)));		//usuwanie małych blobów
	findContours(edges, contours, CV_CHAIN_APPROX_SIMPLE, CV_RETR_TREE);		//kontury

	std::vector<std::vector<Point> > contours_poly(contours.size());		//zmienne z danymi konturów

	boundRect.resize(contours.size());
	center.resize(contours.size());
	radius.resize(contours.size());

	for (int i = 0; i < contours.size(); i++)		//aproksymacja poligonów na prostokąty i kółka
	{
		approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
		boundRect[i] = boundingRect(Mat(contours_poly[i]));
		minEnclosingCircle((Mat)contours_poly[i], center[i], radius[i]);
	}

	Mat drawing = Mat::zeros(edges.size(), CV_8UC3);
	for (int i = 0; i < contours.size(); i++)
	{
		Scalar color = Scalar(255, 0, 0);
		Scalar color2 = Scalar(0, 255, 0);

#ifdef DEBUG
		//drawContours(imgDebug, contours_poly, i, color, 4, 8, std::vector<Vec4i>(), 0, Point());	//rysowanie poligonów
#endif // DEBUG

		
		if (boundRect[i].area() > 600)
		{
			for (int j = 0; j < boundRect.size(); j++)
			{
				float circleArea = 3.1416*radius[i] * radius[i];
				if (circleArea > 6500)		//przypisanie do odpowiednich typów żelka przez porównanie pól figur
				{
					rectangle(imgDebug, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0);	//robak
					detectedWorms.push_back(img(boundRect[i]));		//dodaj do wektora znalezionych
				}
				else if (circleArea < boundRect[i].area())
				{
					if (boundRect[i].contains(boundRect[j].br()) && boundRect[i].contains(boundRect[j].tl()))		//usuwanie zagniezdzonych prostokątków (jesli sa dwa na sobie)
					{
						circle(imgDebug, center[i], (int)radius[i], color, 2, 8, 0);	//krazek
						detectedDiscs.push_back(img(boundRect[i]));		//dodaj do wektora znalezionych
					}
				}
				else
				{
				
					rectangle(imgDebug, boundRect[i].tl(), boundRect[i].br(), color2, 2, 8, 0);	//mis
					detectedBears.push_back(img(boundRect[i]));		//dodaj do wektora znalezionych
				
				}
			}
		}
	}
}

void reset()
{
	bears = 0;
	discs = 0;
	worms = 0;
	for (int i = 0; i < 6; i++)
	{
		discsTypes[i] = 0;
		bearsTypes[i] = 0;
		if (i < 3)
			wormsTypes[i] = 0;
	}
	detectedWorms.clear();
	detectedDiscs.clear();
	detectedBears.clear();
}

vector<int> jellyColor(Mat imageHSV)
{
	Mat maskLightRed;
	Mat maskDarkRed;
	Mat maskOrange;
	Mat maskGreen;
	Mat maskYellow;
	Mat maskWhite;
	vector<Mat> mask;
	vector<int> color;
	vector<int> leadingColour;
	leadingColour.push_back(0);
	leadingColour.push_back(0);
	leadingColour.push_back(0);
	leadingColour.push_back(0);

	Erosion(imageHSV, imageHSV);
	Dilation(imageHSV, imageHSV);

	inRange(imageHSV, Scalar(0, 100, 90), Scalar(7, 255, 255), maskLightRed);
	inRange(imageHSV, Scalar(170, 150, 60), Scalar(180, 255, 255), maskDarkRed);
	inRange(imageHSV, Scalar(10, 100, 20), Scalar(22, 255, 255), maskOrange);
	inRange(imageHSV, Scalar(32, 100, 50), Scalar(58, 255, 255), maskGreen);
	inRange(imageHSV, Scalar(20, 100, 100), Scalar(30, 255, 255), maskYellow);
	inRange(imageHSV, Scalar(0, 0, 0), Scalar(180, 255, 53), maskWhite);

	mask.push_back(maskLightRed);//0
	mask.push_back(maskDarkRed);//1
	mask.push_back(maskOrange);//2
	mask.push_back(maskGreen);//3
	mask.push_back(maskYellow);//4
	mask.push_back(maskWhite);//5

	for (int i = 0; i < 6; i++)
	{
		color.push_back(countPixels(mask[i]));
		if (i == 0 || i == 1)
		{
			leadingColour[2 * i] = color[i];
			leadingColour[2 * i + 1] = i;
			if ((leadingColour[2] > leadingColour[0]))
			{
				int temp1, temp2;
				temp1 = leadingColour[0];
				temp2 = leadingColour[2];
				leadingColour[0] = temp2;
				leadingColour[2] = temp1;
				leadingColour[1] = 1;
				leadingColour[3] = 0;
			}
		}
		if (i > 1 && (color[i] > leadingColour[2]))
		{
			leadingColour[2] = color[i];
			leadingColour[3] = i;

			if ((leadingColour[2] > leadingColour[0]))
			{
				int temp1, temp2, temp3, temp4;
				temp1 = leadingColour[0];
				temp2 = leadingColour[2];
				temp3 = leadingColour[1];
				temp4 = leadingColour[3];

				leadingColour[0] = temp2;
				leadingColour[2] = temp1;
				leadingColour[1] = temp4;
				leadingColour[3] = temp3;
			}
		}
		if (leadingColour[0] < 200 && leadingColour[2] < 300)
		{
			leadingColour[1] = 6;
			leadingColour[3] = 6;
		}
		else if (leadingColour[2] < 200)
		{
			leadingColour[3] = 6;
		}
	}
	return leadingColour;
}
int countPixels(Mat img)
{
	int pixelCount = 0, rows = img.rows, cols = img.cols;

	for (int i = 0; i < rows; ++i)
		for (int k = 0; k < cols; k++)
			if (img.at<uchar>(i, k) == 255) pixelCount++;
	return pixelCount;
}
void Erosion(Mat src, Mat erosion_dst)
{
	int erosion_elem = 2;
	int erosion_size = 9;
	int erosion_type;
	if (erosion_elem == 0) { erosion_type = MORPH_RECT; }
	else if (erosion_elem == 1) { erosion_type = MORPH_CROSS; }
	else if (erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }

	Mat element = getStructuringElement(erosion_type,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));
	erode(src, erosion_dst, element);
}
void Dilation(Mat src, Mat dilation_dst)
{
	int dilation_elem = 2;
	int dilation_size = 11;
	int dilation_type;
	if (dilation_elem == 0) { dilation_type = MORPH_RECT; }
	else if (dilation_elem == 1) { dilation_type = MORPH_CROSS; }
	else if (dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

	Mat element = getStructuringElement(dilation_type,
		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		Point(dilation_size, dilation_size));
	dilate(src, dilation_dst, element);
}
void detectColorsWorms(std::vector<int> detColors, int area)
{
	{
		if (detColors[1] == 4 || detColors[3] == 4)
		{
			wormsTypes[2]++;	//czer-zol
		}
		else if (detColors[1] == 5 || detColors[3] == 5)
		{
			wormsTypes[1]++;	//pom-czar
		}
		else
		{
			wormsTypes[0]++;	//ziel-bia
		}
	}
}
void detectColorsBears(std::vector<int> detColors, int area)
{
	// 0 - ciemnyCzerwony	1-jasnyCzerwony	2-pomaranczowy	3-zolty 4-zielony	5-czarny
	if (detColors[0] > detColors[2])
	{
		if (detColors[1] == 4)
		{
			bearsTypes[4]++;		//zolty
		}
		else if (detColors[1] == 3)
		{
			bearsTypes[3]++;		//zielony
		}
		else if (detColors[1] == 2)
		{
			bearsTypes[2]++;		//pomarancz
		}
		else if (detColors[1] == 1)
		{
			bearsTypes[1]++;		//ciemnyCZ
		}
		else if (detColors[1] == 0)
		{
			bearsTypes[0]++;		//jasnyCZ
		}
		else
		{
			bearsTypes[5]++;		//bialy
		}
	}
	else
	{
		if (detColors[3] == 4)
		{
			bearsTypes[4]++;		//zolty
		}
		else if (detColors[3] == 3)
		{
			bearsTypes[3]++;		//zielony
		}
		else if (detColors[3] == 2)
		{
			bearsTypes[2]++;		//pomarancz
		}
		else if (detColors[3] == 1)
		{
			bearsTypes[1]++;		//ciemnyCZ
		}
		else if (detColors[3] == 0)
		{
			bearsTypes[0]++;		//jasnyCZ
		}
		else
		{
			bearsTypes[5]++;		//bialy
		}
	}
}
void detectColorsDiscs(std::vector<int> detColors, int area)
{
	if (detColors[0] > detColors[2])
	{
		if (detColors[1] == 4)
		{
			discsTypes[4]++;		//zolty
		}
		else if (detColors[1] == 3)
		{
			discsTypes[3]++;		//zielony
		}
		else if (detColors[1] == 2)
		{
			discsTypes[2]++;		//pomarancz
		}
		else if (detColors[1] == 1)
		{
			discsTypes[1]++;		//ciemnyCZ
		}
		else if (detColors[1] == 0)
		{
			discsTypes[0]++;		//jasnyCZ
		}
		else
		{
			discsTypes[5]++;		//bialy
		}
	}
	else
	{
		if (detColors[3] == 4)
		{
			discsTypes[4]++;		//zolty
		}
		else if (detColors[3] == 3)
		{
			discsTypes[3]++;		//zielony
		}
		else if (detColors[3] == 2)
		{
			discsTypes[2]++;		//pomarancz
		}
		else if (detColors[3] == 1)
		{
			discsTypes[1]++;		//ciemnyCZ
		}
		else if (detColors[3] == 0)
		{
			discsTypes[0]++;		//jasnyCZ
		}
		else
		{
			discsTypes[5]++;		//bialy
		}
	}
}
void detectJellies()
{
	for (int i = 0; i < detectedWorms.size(); i++)
	{
		if (i > 0 && detectedWorms[i].rows == detectedWorms[i - 1].rows)
		{
			continue;
		}
		worms++;
		Mat temp;
		cvtColor(detectedWorms[i], temp, CV_BGR2HSV);
		detectColorsWorms(jellyColor(temp), detectedWorms[i].rows * detectedWorms[i].cols);
	}
	for (int i = 0; i < detectedBears.size(); i++)
	{
		if (i > 0 && detectedBears[i].rows == detectedBears[i - 1].rows)
		{
			continue;
		}
		bears++;
		Mat temp;
		cvtColor(detectedBears[i], temp, CV_BGR2HSV);
		imshow("Display", detectedBears[i]);
		std::vector<int> lead_clr = jellyColor(temp);
		cout << lead_clr[1] << "," << lead_clr[3] << endl;
		while (waitKey(100) == -1);
		detectColorsBears(lead_clr, detectedBears[i].rows * detectedBears[i].cols);
	}
	for (int i = 0; i < detectedDiscs.size(); i++)
	{
		if (i > 0 && detectedDiscs[i].rows == detectedDiscs[i - 1].rows)
		{
			continue;
		}
		discs++;
		Mat temp;
		cvtColor(detectedDiscs[i], temp, CV_BGR2HSV);

		detectColorsDiscs(jellyColor(temp), detectedDiscs[i].rows * detectedDiscs[i].cols);
	}
	if (bearsTypes[1] == 0)
	{
		bearsTypes[1] = bearsTypes[0] / 2;
		bearsTypes[0] = bearsTypes[0] - bearsTypes[1];
	}
	if (discsTypes[1] == 0)
	{
		discsTypes[1] = discsTypes[0] / 2;
		discsTypes[0] = discsTypes[0] - discsTypes[1];
	}
	if (discsTypes[5] == 0)
	{
		discsTypes[5] = bearsTypes[5] / 2;
		bearsTypes[5] = bearsTypes[5] - discsTypes[5];
	}
}


static void onMouse(int event, int x, int y, int, void*)
{
	if (event == EVENT_LBUTTONDOWN)
	{
		for(int i = 0; i < boundRect.size();i++)
			if (boundRect[i].contains(Point(x, y)))
			{
				putText(imgDebug, "CA: " + std::to_string(radius[i] * radius[i] * 3.1416), Point(x, y), FONT_HERSHEY_DUPLEX, 0.5, Scalar(0, 255, 0));
				putText(imgDebug, "RA: " + std::to_string(boundRect[i].area()), Point(x, y + 15), FONT_HERSHEY_DUPLEX, 0.5, Scalar(0, 255, 0));
				imshow("Display", imgDebug);
				return;
			}
	}
}

int main(int argc, char** argv)
{
#ifdef DEBUG
	namedWindow("Display");
	setMouseCallback("Display", onMouse,0);
	img = imread("C:/Users/mafis/Desktop/Images/img_059.jpg");
	if(img.rows > img.cols)
		resize(img, img, Size(600, 800));
	else
		resize(img, img, Size(800, 600));
	img.copyTo(imgDebug);
	cvtColor(img, imgGray, CV_BGR2GRAY);
	FindZelki(0,0);
	detectJellies();

	std::string resultToOutput;
	for (int j = 0; j < 6; j++)
		resultToOutput = resultToOutput + std::to_string(bearsTypes[j]) + " ";
	for (int j = 0; j < 6; j++)
		resultToOutput = resultToOutput + std::to_string(discsTypes[j]) + " ";
	for (int j = 0; j < 3; j++)
		if (j == 2)
			resultToOutput = resultToOutput + std::to_string(wormsTypes[j]);
		else
			resultToOutput = resultToOutput + std::to_string(wormsTypes[j]) + " ";
	cout << resultToOutput;
	imshow("Display", imgDebug);
	while(waitKey(20) != 27);
#else
	std::string imageLocation;
	std::string outputLocation;
	std::string resultToOutput;
	if (argc == 3)
	{
		imageLocation = argv[1];
		outputLocation = argv[2];
		cout << "Searching for images in: " << imageLocation << endl << "Output will be saved in: " << outputLocation << endl << endl;;
	}
	else
	{
		cout << "Wrong parameters!" << endl;
		cout << "Use notation similar to './program /path/to/pics /path/to/output' " << endl;
		return -1;
	}
	std::fstream outputFile;
	outputFile.open(outputLocation, std::ios::out);
	for (int i = 0; i < 999; i++)
	{
		std::string fileName;
		if (i <= 9)
			fileName = imageLocation + "/img_00" + std::to_string(i) + ".jpg";
		else if (i <= 99)
			fileName = imageLocation + "/img_0" + std::to_string(i) + ".jpg";
		else
			fileName = imageLocation + "/img_" + std::to_string(i) + ".jpg";
		img = imread(fileName);
		if (img.cols == 0 || img.rows == 0)		//check if image seems correct
			continue;
		else
		{
			if (img.rows > img.cols)
				resize(img, img, Size(600, 800));
			else
				resize(img, img, Size(800, 600));
			cvtColor(img, imgGray, CV_BGR2GRAY);
			FindZelki(0,0);
			detectJellies();
			if (i <= 9)
				resultToOutput = "img_00" + std::to_string(i) + ".jpg ";
			else if (i <= 99)
				resultToOutput = "img_0" + std::to_string(i) + ".jpg ";
			else
				resultToOutput = "img_" + std::to_string(i) + ".jpg ";
			for (int j = 0; j < 6; j++)
					resultToOutput = resultToOutput + std::to_string(bearsTypes[j]) + " ";
			for (int j = 0; j < 6; j++)
					resultToOutput = resultToOutput + std::to_string(discsTypes[j]) + " ";
			for (int j = 0; j < 3; j++)
				if (j == 2)
					resultToOutput = resultToOutput + std::to_string(wormsTypes[j]);
				else
					resultToOutput = resultToOutput + std::to_string(wormsTypes[j]) + " ";

			resultToOutput = resultToOutput + "\n";
			cout << resultToOutput;
			outputFile << resultToOutput;
			reset();
		}
	}
#endif  DEBUG
	return 0;
}

