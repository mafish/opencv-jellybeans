cmake_minimum_required(VERSION 2.8)

project(simplecv)

find_package(OpenCV)
find_package(ZBar0)
add_executable(${PROJECT_NAME} "src/main.cpp")

target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS} ${ZBAR_LIBRARIES})

